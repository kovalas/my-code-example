'use strict';

var mongo = require('../support/database').reference,
    SelectableEntity = require('./selectableEntity'),
    activity = require('../activity/activity'),
    q = require('q'),
    f = require('../support/f');

function ParticipantsProvider() {

}

ParticipantsProvider.create = function() {
  return new ParticipantsProvider();
};

ParticipantsProvider.prototype = new SelectableEntity();

ParticipantsProvider.prototype.craftCode = function (code) {
  this.craftCodeCriteria = code;
  return this;
};

ParticipantsProvider.prototype.agglomerationId = function (id) {
  this.agglomerationIdCriteria = id;
  return this;
};

ParticipantsProvider.prototype.extract = function () {
  var criteria = {};

  if (this.craftCodeCriteria) {
    criteria.craftCode = this.craftCodeCriteria;
  }
  if (this.agglomerationIdCriteria) {
    criteria.agglomerationId = this.agglomerationIdCriteria;
  }

  return mongo.from('workshop_participants')
    .find(criteria)
    .sort({'createdOn': -1})
    .limit(this.limit())
    .skip(this.offset())
    .toArray()
    .then(this.convertDate.bind(this, 'createdOn'))
    .then(function (participants) {
      var promises = [];
      participants.forEach(function (participant) {
        promises.push(activity.loadObjectsById([f.wrapId(participant.placeId)]).then(function (objects) {
          participant.place = objects[0];
        }));
      });

      return q.all(promises).then(function (){
        return participants;
      });
    })
    .then(function (results) {
      return mongo.from('workshop_participants').count(criteria).then(function (cnt) {
        return {
          count: cnt,
          items: results
        };
      });
    });
};

module.exports = ParticipantsProvider;
