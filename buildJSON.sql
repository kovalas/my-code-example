SELECT concat(
    '[',
    array_to_string(
        array(
            SELECT concat('{',
                          '"id": ', id, ',',
                          '"guide": ',
                          concat('{',
                                 concat('"name": "', guide.name, '"'), ',',
                                 concat('"intro_image_id": "', guide.intro_image_id, '"'), ',',
                                 concat('"outro_image_id": "', guide.outro_image_id, '"'), ',',
                                 concat('"agglomerations": ', '[', array_to_string(
                                     array(
                                         select agglomeration_id
                                         from guide__agglomeration
                                         where guide_id=guide.id), ','
                                 ) , ']'),
                                 '}'
                          ),
                          '}')
                     AS result
            FROM workshop_guides AS wg
              JOIN guide ON guide.id = wg.guide_id
              left join guide__agglomeration as ga on ga.guide_id=guide.id
            where guide.published=true
        ),
        ','),
    ']'
);