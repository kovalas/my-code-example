'use strict';

var mongo = require('../support/database').reference,
    SelectableEntity = require('./selectableEntity'),
    activity = require('../activity/activity'),
    guideUtils = require('../activity/guides'),
    q = require('q'),
    _ = require('lodash'),
    f = require('../support/f');

function GuidesProvider() {

}

GuidesProvider.create = function () {
  return new GuidesProvider();
};

GuidesProvider.prototype = new SelectableEntity();

GuidesProvider.prototype.agglomerationId = function (id) {
  this.agglomerationIdCriteria = id;
  return this;
};

GuidesProvider.prototype.extract = function (req) {
  var criteria = {},
      self = this;

  if (this.agglomerationIdCriteria) {
    criteria = {
      agglomerations: {
        $elemMatch: {
          $eq: mongo.ObjectId(f.wrapId(this.agglomerationIdCriteria))
        }
      }
    };
  }

  return mongo.from('guides')
      .find(criteria, {_id: 1})
      .toArray()
      .then(function (availableGuides) {
        return availableGuides.map(function (guide) {
          return guide._id;
        });
      })
      .then(makeQuery);

  function makeQuery(availableList) {
    var criteria = {_id: {$in: availableList}};
    return mongo.from('workshop_guides')
      .find(criteria)
      .sort({'createdOn': -1})
      .limit(self.limit())
      .skip(self.offset())
      .toArray()
      .then(self.convertDate.bind(null, 'createdOn'))
      .then(function (guides) {
        var promises = [];
        guides.forEach(function (guide) {
          promises.push(mongo.from('guides').findOne({_id: guide._id})
            .then(function (fullGuide) {
                return guideUtils.attachProfilesToGuides([fullGuide], req)
                  .then(function (guides) {
                    return guideUtils.attachMultiAuthorsToGuides(guides, req);
                  })
                  .then(function (guides) {
                    return _.omit(guides[0], 'blocks');
                  })
                  .then(function (fullGuide) {
                    return {
                      createdOn: guide.createdOn,
                      guide: fullGuide
                    };
                });
            })

          );
        });

        return q.all(promises)
      })
      .then(function (results) {
          return mongo.from('workshop_guides').count(criteria)
              .then(function (cnt) {
                  return {
                    count: cnt,
                    items: results
                  }
              });
      })
  }

};

module.exports = GuidesProvider;

