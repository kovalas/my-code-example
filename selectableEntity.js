'use strict';

function SelectableEntity() {
  this._limit = 4;
  this._offset = 0;
}

SelectableEntity.prototype.limit = function (limit) {
  if (limit !== undefined) {
    this._limit = limit;
    return this;
  }
  return this._limit;
};

SelectableEntity.prototype.offset = function (offset) {
  if (offset !== undefined) {
    this._offset = offset;
    return this;
  }
  return this._offset;
};

SelectableEntity.prototype.convertDate = function (dateFieldName, results) {
  results.forEach(function (event) {
    event[dateFieldName] = event[dateFieldName].getTime();
  });
  return results;
}

module.exports = SelectableEntity;
