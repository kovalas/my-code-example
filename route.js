'use strict';

var eventsProvider = require('./eventsProvider'),
    participantsProvider = require('./participantsProvider'),
    guidesProvider = require('./guidesProvider'),
    guides = require('../activity/guides'),
    mongo = require('../support/database').reference;

module.exports = function (app) {

  app.get('/workshops/events/upcoming', function (req, res) {
    eventsProvider.create().upcoming()
        .then(function (results) {
          res.status(200).header("Content-Type", "application/json; charset=utf-8").send(results.items);
        })
        .catch(function (err) {
          res.status(500).send(err);
        });
  });

  app.get('/workshops/events/passed', function (req, res) {
    var limit = parseInt(req.param('limit')),
        offset = parseInt(req.param('offset'));
    eventsProvider.create().limit(limit).offset(offset).passed()
        .then(function (results) {
          res.status(200).header("Content-Type", "application/json; charset=utf-8").send(results);
        })
        .catch(function (err) {
          res.status(500).send(err);
        });
  });

  app.get('/workshops/participant', function (req, res) {
    var limit = parseInt(req.param('limit')),
        offset = parseInt(req.param('offset')),
        agglomerationId = parseInt(req.param('agglomerationId'));

    participantsProvider.create()
        .craftCode(req.param('craftCode'))
        .agglomerationId(agglomerationId)
        .limit(limit)
        .offset(offset)
        .extract()
        .then(function (results) {
          res.status(200).header("Content-Type", "application/json; charset=utf-8").send(results);
        })
        .catch(function (err) {
          res.status(500).send(err);
        });
  });

  app.get('/workshops/guides', function (req, res) {
    var limit = parseInt(req.param('limit')),
        offset = parseInt(req.param('offset')),
        agglomerationId = parseInt(req.param('agglomerationId'));

    guidesProvider
        .create()
        .agglomerationId(agglomerationId)
        .limit(limit)
        .offset(offset)
        .extract(req)
        .then(function (results) {
          res.status(200).header("Content-Type", "application/json; charset=utf-8").send(results);
        })
        .catch(function (err) {
          res.status(500).send(err);
        });
  });

  app.get('/workshops/crafts', function (req, res) {
    mongo.from('workshop_crafts').find().toArray().then(function (results) {
      res.status(200).header("Content-Type", "application/json; charset=utf-8").send(results);
    })
        .catch(function (err) {
          res.status(500).send(err);
        });
  });

};
