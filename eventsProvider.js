'use strict';

var mongo = require('../support/database').reference,
    SelectableEntity = require('./selectableEntity');

function EventProvider() {
  var d = new Date();
  this.today = new Date(d.getFullYear(), d.getMonth(), d.getDate());
}

EventProvider.create = function () {
  return new EventProvider();
};

EventProvider.prototype = new SelectableEntity();

EventProvider.prototype.passed = function () {
  return this.extractEvents(null, this.today, 'desc');
};

EventProvider.prototype.upcoming = function () {
  return this.extractEvents(this.today, null, 'asc');
};

EventProvider.prototype.extractEvents = function (dateFrom, dateTo, sort) {
  var criteria = {
    date: {}
  };
  if (dateTo) {
    criteria.date.$lt = dateTo;
  }
  if (dateFrom) {
    criteria.date.$gte = dateFrom;
  }
  return mongo.from('workshop_events')
      .find(criteria)
      .sort({'date': sort === 'desc' ? -1 : 1})
      .limit(this.limit())
      .skip(this.offset())
      .toArray()
      .then(this.convertDate.bind(this, 'date'))
      .then(function (results) {
        return mongo.from('workshop_events').count(criteria).then(function (cnt) {
          return {
            count: cnt,
            items: results
          };
        });
      });
};

module.exports = EventProvider;
